# Pi Media Player (PiMP) image built using the Yocto Project

Pi Media Player (PiMP) is an embedded media player with Internet Radio and UPnP
Media Renderer functionality.  It runs on a
[Raspberry Pi Zero](https://www.raspberrypi.org/products/pi-zero/) single-board
computer with added audio output card, USB WiFi dongle, and custom circuitry to 
implement a push-button user interface and ON/OFF power switching.  This git 
repository *build-pimp* contains a [Yocto Project](https://www.yoctoproject.org/) 
build directory that creates a custom Linux image for the device, based on the 
Yocto project's *poky* reference distribution, the *meta-raspberrypi* board 
supportpackage layer, and some other layers from the
[OpenEmbedded](http://www.openembedded.org/wiki/Main_Page) source tree.

The *meta-pimp* subdirectory contains metadata for the project-specific *pimp*
layer, which contains bit-bake recipes for the custom Linux image and all the
constituent libraries and applications  for which there are no existing recipes
in the *poky* or *openembedded* source trees.  

The project documentation describes what hardware you need for Pi Media Player
and how to build the software image, configure it for use on your own home
network, and use the resulting media player.


## Introduction

Pi Media Player (PiMP) is a UPnP Media Renderer.  It is implemented on a
Raspberry Pi Zero board, with a push-button user interface to turn power on
and off, and to switch between UPnP Media Renderer and Internet Radio operation.

PiMP is based on the Yocto Project's meta-raspberrypi layer, with additions
to enable WiFi communication using the Broadcom-based Raspberry Pi WiFi USB
dongle and Pimoroni [Phat DAC](https://shop.pimoroni.com/products/phat-dac)
audio output card.


## Adding the meta-pimp layer to your Yocto Project build

In order to use the meta-pimp layer, you need to make the build system aware of
it.  Assuming the meta-pimp layer exists at the top-level of your yocto build 
tree, you can add it to the build system by adding the location of the meta-pimp 
layer to bblayers.conf, along with any other layers needed. e.g.:

    BBLAYERS ?= " \  
      ${HOME}/yocto/poky/meta \  
      ${HOME}/yocto/poky/meta-poky \  
      ${HOME}/yocto/poky/meta-yocto-bsp \  
      ${HOME}/yocto/poky/meta-raspberrypi \  
      ${HOME}/yocto/poky/meta-pimp \  
      "

### Dependencies

The meta-pimp layer depends on:

  URI: <git://git.openembedded.org/bitbake>  
  branch: morty  

  URI: <git://git.yoctoproject.org/poky>  
  layers: meta  
  branch: morty  

  URI: <git://git.yoctoproject.org/meta-raspberrypi>  
  layers: meta-raspberrypi  
  branch: morty


## UPnP Media Renderer implementation

The UPnP media renderer is implemented using the Linux Music Player Daemon
[mpd](https://mpd.readthedocs.io/en/stable/user.html) and the UPnP front-end 
[upmpdcli](https://www.lesbonscomptes.com/upmpdcli/index.html) that transforms 
UPnP communications with the UPnP control point into _mpd_ messages that control 
the _mpd_ server.


## User Interface and power switching hardware

The user interface consists of a single push-button switch and a LED. The switch
has multiple functions:

- Turn power on and off.
- Switch between UPnP media renderer and internet radio modes of operation.
- Change radio station.

The hardware circuit diagram is shown below. The git repository also includes a
PDF version of the circuit diagram: [doc/pimp-lite-schematic.pdf](doc/pimp-lite-schematic.pdf)

![circuit schematic](doc/pimp-lite-schematic.svg)

The LED is controlled by GPIO17, which is driven high at boot-time to turn the 
LED on and remains high while the Pi Media Player is powered up. GPIO17 goes low 
when the Pi Media Player is powered off, extinguishing the LED.

The push-button switch pulls down GPIO4, which is configured to have a pull-up 
resistor. Software reads the button state to implement the Pi Media Player's 
user interface.

The push button is also used to power on the Pi Media Player. While the unit is
powered off, GPIO17 is low and GPIO4 is high. Therefore the output of NOR gate 
U1a is low and transistor Q1 is switched off. Pressing push-button switch SW1
causes the output of NOR gate U1a to go high and transistor Q1 to be switched 
on. That pulls the Raspberry Pi's _'RUN'_ pin to be pulled to GND, which
generates a chip reset that causes the processor to boot up.


## User Interface and power switching software

The user interface and power switching software is implemented by the _rpionoff_
Bitbake layer, that is defined in subdirectory _meta-pimp/recipes-bsp/rpionoff_.
It is a C++ application, whose _main()_ function is in the file
_meta-pimp/recipes-bsp/rpionoff/files/rpionoff.cpp_.

When in internet radio mode, the _rpionoff_ application invokes the script 
[radio.sh](meta-pimp/recipes-core/pimp-files/files/radio.sh).

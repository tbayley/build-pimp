/*
 * Power On/Off Switching for Raspberry Pi (rpionoff)
 * Version 1.0.0
 * https://bitbucket.org/tbayley/rpionoff
 *
 * Copyright Tony Bayley 2017.
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file COPYING.BOOST-1_0 or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 *
 * Filename:   time_service.hpp
 * Created on: 6 Mar 2017
 *
 * The time service is an abstraction layer giving access to OS time functions.
 * Indirect access to OS time functions via the "time_service" interface enables
 * implementation of a mock time service for unit testing, by which the results
 * of time functions may be programmatically manipulated.
 *
 */

#ifndef TONY_BAYLEY_TIME_SERVICE_HPP_
#define TONY_BAYLEY_TIME_SERVICE_HPP_

#include <time.h>           // timespec
#include <unistd.h>         // useconds_t

/*!
@brief Namespace for Pi Media Player
@see https://bitbucket.org/tbayley/pimp
*/

namespace PIMP
{

/*!
 * @brief Time service class
 *
 * The time service is an abstraction layer giving access to OS time
 * functions.  Indirect access to OS time functions via the "time_service"
 * interface enables implementation of a mock time service for unit tests,
 * by which the results of time functions may be programmatically
 * manipulated.
 */
class TimeService {

public:

     /// TimeService constructor.
     TimeService();


     /// TimeService destructor.
     virtual ~TimeService() = default;


    /*!
     * @brief   Get the elapsed time value.
     *
     *          Return the time since the TimeService object was created,
     *          or was last reset, in seconds.
     *
     * @return  Elapsed time in seconds.
     */
    virtual double get_elapsed_time();


    /// Reset the elapsed time to zero.
    virtual void reset();


    /*!
     * @brief   Sleep the current thread for a specified number of seconds.
     *
     *          Delays implemented by calling TimeService::sleep instead of
     *          the standard library's sleep function can be overridden in
     *          unit tests, to speed up test time.
     *
     * @param   time    Time to sleep in seconds.
     */
    virtual void sleep(unsigned int time);


    /*!
     * @brief   Sleep the current thread for a specified number of microseconds.
     *
     *          Delays implemented by calling TimeService::usleep instead of
     *          the standard library's usleep function can be overridden in
     *          unit tests, to speed up test time.
     *
     * @param   time    Time to sleep in microseconds.
     */
    virtual void usleep(useconds_t time);


private:
    /// Time at which the TimeService object was created, or was last reset.
    struct timespec start_time;

    /*!
     * @brief  Calculate the time difference between two timespec values.
     *
     * @param  t_new The most recent timespec value.
     * @param  t_old The oldest timespec value.
     * @return       Time difference in seconds.
     */
    inline double timespec_diff(const struct timespec t_new, const struct timespec t_old);
};

} /* namespace PIMP */

#endif /* TONY_BAYLEY_TIME_SERVICE_HPP_ */

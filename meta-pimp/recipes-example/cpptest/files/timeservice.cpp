/*
 * Power On/Off Switching for Raspberry Pi (rpionoff)
 * Version 1.0.0
 * https://bitbucket.org/tbayley/rpionoff
 *
 * Copyright Tony Bayley 2017.
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file COPYING.BOOST-1_0 or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 *
 * Filename:   time_service.cpp
 * Created on: 6 Mar 2017
 *
 * The time service is an abstraction layer giving access to OS time functions.
 * Indirect access to OS time functions via the "time_service" interface enables
 * implementation of a mock time service for unit testing, by which the results
 * of time functions may be programmatically manipulated.
 *
 */

#include "timeservice.hpp"
#include <time.h>           // CLOCK_MONOTONIC, timespec
#include <unistd.h>         // sleep, usleep

namespace PIMP
{

// TimeService constructor
TimeService::TimeService(){
    // initialise elapsed time
    clock_gettime(CLOCK_MONOTONIC, &start_time);
}

// Return elapsed time in seconds
double TimeService::get_elapsed_time(){
    struct timespec time_now;
    clock_gettime(CLOCK_MONOTONIC, &time_now);
    return timespec_diff(time_now, start_time);
}

// Reset the elapsed time to zero.
void TimeService::reset() {
    // re-initialise elapsed time
    clock_gettime(CLOCK_MONOTONIC, &start_time);
}

// Sleep the current thread for a specified number of seconds.
void TimeService::sleep(unsigned int time) { ::sleep(time); }

// Sleep the current thread for a specified number of microseconds.
void TimeService::usleep(useconds_t time) { ::usleep(time); }

// Calculate the time difference between two timespec values.
inline double TimeService::timespec_diff(const struct timespec t_new, const struct timespec t_old){
    return (double)(t_new.tv_sec - t_old.tv_sec) +
           (double)(t_new.tv_nsec - t_old.tv_nsec) / 1000000000.0;
}

} /* namespace PIMP */

/*
 * Power On/Off Switching for Raspberry Pi (rpionoff)
 * Version 1.0.0
 * https://bitbucket.org/tbayley/rpionoff
 *
 * Copyright Tony Bayley 2017.
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file COPYING.BOOST-1_0 or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 *
 * Filename:   cpptest.cpp
 * Created on: 19 Mar 2017
 *
 * C++ test program for Yocto Project.
 *
 */

#define DEBUG

#include "GPIO.hpp"
#include "debug.hpp"

#include <unistd.h>      // usleep

using PIMP::GPIO;
using PIMP::OUTPUT;
using PIMP::LOW;  using PIMP::HIGH;
using PIMP::TimeService;

/*!
 * @brief Main entry point of C++ test application.
 * @return  0: Successful shutdown, -1: Error condition.
 */
int main() {

    debug_cout("Running cpptest");

    // Initialise LED on GPIO17.
    GPIO gpio_led(17);
    if (!gpio_led.gpio_is_valid()) {
        debug_cerr("Invalid LED GPIO");
        return -1;
    }
    gpio_led.set_direction(OUTPUT);
    gpio_led.set_value(LOW);
    debug_cout("LED initialised");
    
    // flash LED for 10 seconds
    for (int i=0; i<20; i++) {
        gpio_led.toggle_output();
        usleep(500000); // 0.5 second delay
    }
    
    debug_cout("Ending cpptest");
    gpio_led.set_value(LOW);
    return 0;
}


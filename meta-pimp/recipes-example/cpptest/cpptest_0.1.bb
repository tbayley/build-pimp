SUMMARY = "C++ test program for Yocto Project."
SECTION = "example"

LICENSE = "BSL-1.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSL-1.0;md5=65a7df9ad57aacf825fd252c4c33288c"

S = "${WORKDIR}"

SRC_URI = "file://cpptest.cpp \
           file://GPIO.cpp \
           file://GPIO.hpp \
           file://timeservice.cpp \
           file://timeservice.hpp \
           file://debug.hpp \
"

do_compile() {
    ${CXX} -pthread ${CXXFLAGS} ${LDFLAGS} \
    cpptest.cpp \
    GPIO.cpp \
    timeservice.cpp \
    -o ${PN}
}

# Install application as /usr/bin/cpptest
do_install() {
    install -m 0755 -d ${D}${bindir}
    install -m 0755 ${PN} ${D}${bindir}
}


# Fix the QA warning "mpd RDEPENDS on libmpdclient but it isn't a building
# dependency.  The potential problem is that if no other package DEPENDS on
# libmpdclient it will not be included in the image.  The simple solution is to
# add libmpdclient to DEPENDS even if it is not needed at build time!
DEPENDS += " libmpdclient"

# Prepend the path for the updated /etc/mpd.conf file to the original
# file search path, so that the updated file overrides the original one.
# The pid_file variable is set for use in init.d start/stop scripts.
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

# Include a script /etc/init.d/mpd to autostart mpd at boot time.
SRC_URI += " file://mpd.init"

do_install_append() {
    install -d ${D}${sysconfdir}/init.d
    install -m 0755 ${WORKDIR}/mpd.init ${D}${sysconfdir}/init.d/mpd
}

inherit update-rc.d

INITSCRIPT_NAME = "mpd"
INITSCRIPT_PARAMS = "defaults 25"


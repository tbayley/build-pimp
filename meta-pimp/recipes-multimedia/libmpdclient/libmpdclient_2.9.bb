SUMMARY = "Music Player Daemon (mpd) client library"
HOMEPAGE = "https://www.musicpd.org/libs/libmpdclient/"
SECTION = "libs"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=06b9dfd2f197dc514d8ef06549684b77"

SRC_URI = "http://www.musicpd.org/download/libmpdclient/2/libmpdclient-2.9.tar.xz"
SRC_URI[md5sum] = "4b101a36e5c0cf967dc063391260ddbf"
SRC_URI[sha256sum] = "7cc97ba560e91addafe5975f7e7da04b95d6710750e47b3367fa0d8c3b7699ca"

inherit autotools gettext pkgconfig

FILES_${PN} += "${datadir}/vala/vapi/libmpdclient.vapi"

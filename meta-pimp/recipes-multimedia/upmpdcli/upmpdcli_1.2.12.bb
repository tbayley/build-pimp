SUMMARY = "A UPnP Media Renderer front-end for Music Player Daemon (mpd)."
HOMEPAGE = "http://www.lesbonscomptes.com/upmpdcli/"
SECTION = "console/multimedia"

DESCRIPTION = "Upmpdcli is an UPnP Media Renderer front-end to Music Player \
               Daemon (MPD). It supports both pure UPnP and the OpenHome \
               ohMedia services. In OpenHome mode, it supports radio streams. \
               For guidance on building and using the software, \
               see http://www.lesbonscomptes.com/upmpdcli/upmpdcli.html."

LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING;md5=94d55d512a9ba36caa9b7df079bae19f"

SRC_URI = "\
    http://www.lesbonscomptes.com/upmpdcli/downloads/upmpdcli-1.2.12.tar.gz \
    file://upmpdcli.conf \
    file://upmpdcli.init \
    file://rpi-icon.png \
"
SRC_URI[md5sum] = "1896e3aad7ab9303a584cd23766c6cde"
SRC_URI[sha256sum] = "20d97c2e5ff6911e1e1538edb1487823479802cba6bd4f4c4a74035edf61cb26"

inherit autotools gettext pkgconfig

# build in the source directory to suppress do_install errors
B = "${S}"

DEPENDS = "libupnpp libupnp curl libmpdclient expat libmicrohttpd jsoncpp"
PREFERRED_VERSION_libupnp = "1.6%"

# Autotools package configuration options
EXTRA_OECONF += "--prefix=/usr --sysconfdir=/etc"

do_install_append () {
    # install customized configuration files
    install -d ${D}${sysconfdir}/init.d
    install -m 0755 ${WORKDIR}/upmpdcli.init ${D}${sysconfdir}/init.d/upmpdcli
    install -m 0755 ${WORKDIR}/upmpdcli.conf ${D}${sysconfdir}/upmpdcli.conf
    install -d ${D}${datadir}/upmpdcli
    install -m 0755 ${WORKDIR}/rpi-icon.png ${D}${datadir}/upmpdcli/rpi-icon.png

    # fix "host-user-contaminated" warnings for script files extracted from
    # web.tar.gz and rdpl2stream.tar.gz archives to /usr/share/upmpdcli
    chown -R root:root ${D}${datadir}/upmpdcli/web
    chown -R root:root ${D}${datadir}/upmpdcli/rdpl2stream
}

inherit update-rc.d

INITSCRIPT_NAME = "upmpdcli"
INITSCRIPT_PARAMS = "defaults 30"

FILES_${PN} += "\
    ${sysconfdir}/upmpdcli.conf \
    ${datadir}/upmpdcli/rpi-icon.png \
"


# Apply patch to fix build failure for Raspberry Pi Zero, which does not have 
# Neon FPU. See https://github.com/kcat/openal-soft/issues/54 for details.

FILESEXTRAPATHS_append := ":${THISDIR}/openal-soft_1.15.1"
SRC_URI += "file://fix_neon_bug.patch"


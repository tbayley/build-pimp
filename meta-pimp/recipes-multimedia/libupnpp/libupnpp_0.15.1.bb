SUMMARY = "libupnpp : a C++ wrapper for libupnp"
HOMEPAGE = "https://github.com/medoc92/libupnpp"
SECTION = "libs"

DESCRIPTION = "libupnpp defines useful C++ objects over libupnp and can be \
               used to create both devices and control points. It is shared \
               by upmpdcli and upplay."

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=94d55d512a9ba36caa9b7df079bae19f"

SRC_URI = "http://www.lesbonscomptes.com/upmpdcli/downloads/libupnpp-0.15.1.tar.gz"
SRC_URI[md5sum] = "0794f7497cbfa3fad2228abf41efbe33"
SRC_URI[sha256sum] = "c558e6285d61485e656bc973511396665b68b1d5cfa34db5fa4e64e0f2026c3a"

inherit autotools gettext pkgconfig

DEPENDS = "libupnp curl"
PREFERRED_VERSION_libupnp = "1.6%"

# Autotools package configuration options
EXTRA_OECONF += "--prefix=/usr --sysconfdir=/etc"


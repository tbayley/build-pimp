# The Eclipse Target Communication Framework (TCF) agent fails to shutdown, 
# which delays the system poweroff by 40 seconds.  This is fixed by modifying
# the "stop" script to call start-stop-daemon instead of killproc.  For details
# see the following post:
#
#   https://forums.xilinx.com/t5/Embedded-Linux/PetaLinux-2016-4-can-t-kill-tcf-agent-on-shutdown/td-p/744256


# Prepend the path for the updated /etc/init.d/tcf-agent file to the original
# file search path, so that the updated file overrides the original one.

FILESEXTRAPATHS_prepend := "${THISDIR}/tcf-agent:"


SUMMARY = "Power on/off switching for Raspberry Pi Media Player (PiMP)."
HOMEPAGE = "https://bitbucket.org/tbayley/pimp"
SECTION = "bsp"

DESCRIPTION = "Power on/off switching for Raspberry Pi Media Player (PiMP), \
               using a single pushbutton switch. Power on status is indicated \
               by illumination of an LED."

LICENSE = "BSL-1.0 & EUPL-1.1"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSL-1.0;md5=65a7df9ad57aacf825fd252c4c33288c \
                    file://${COMMON_LICENSE_DIR}/EUPL-1.1;md5=3f12b8134016fd7ba5a010afd690abaa \
                   "

# rpionoff uses the script radio.sh that is installed by pimp-files recipe
RDEPENDS_${PN} = "pimp-files espeak sox"

S = "${WORKDIR}"

SRC_URI = "file://rpionoff.cpp \
           file://button.cpp \
           file://button.hpp \
           file://GPIO.cpp \
           file://GPIO.hpp \
           file://led.cpp \
           file://led.hpp \
           file://timeservice.cpp \
           file://timeservice.hpp \
           file://debug.hpp \
           file://rpionoff.init \
"

do_compile() {
    ${CXX} -pthread ${CXXFLAGS} ${LDFLAGS} \
    rpionoff.cpp \
    button.cpp \
    GPIO.cpp \
    led.cpp \
    timeservice.cpp \
    -o ${PN}
}

do_install() {
    # application /usr/bin/rpionoff
    install -m 0755 -d ${D}${bindir}
    install -m 0755 ${PN} ${D}${bindir}
    # init script to run rpionoff as daemon
    install -d ${D}${sysconfdir}/init.d
    install -m 0755 ${WORKDIR}/rpionoff.init ${D}${sysconfdir}/init.d/rpionoff
}

# Configure rpionoff daemon to autostart at boot time
inherit update-rc.d
INITSCRIPT_NAME = "rpionoff"
INITSCRIPT_PARAMS = "defaults 50"


/*
 * Power On/Off Switching for Raspberry Pi (rpionoff)
 * Version 1.0.0
 * https://bitbucket.org/tbayley/rpionoff
 *
 * Copyright Tony Bayley 2017.
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file COPYING.BOOST-1_0 or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 *
 * Filename:   rpionoff.cpp
 * Created on: 13 Mar 2017
 *
 * A single pushbutton is used to start and stop Linux running on the Raspberry
 * Pi single board computer. Shut down requires a long press (greater than 1
 * second) and release, while in UPnP renderer mode.  Once the system has been 
 * shut down via a button press the power supply can be unplugged, if required.
 *
 * A single-button UI was later added to switch between UPnP media rendering
 * and internet radio modes of operation.  A bit of a kludge, but it quickly 
 * implements the basic functionality I want!  The UI is handled here because 
 * my current GPIO class does not allow a single GPIO / push-button to be
 * simultaneously accessed from two separate programs.  Radio functionality is
 * implemented by the shell script /home/root/radio.sh, which is included in 
 * the pimp-files bitbake recipe.
 */

#ifndef UNIT_TEST

#include "button.hpp"
#include "debug.hpp"
#include "led.hpp"

#include <queue>
#include <stdlib.h>         // system
#include <unistd.h>         // usleep

using std::queue;
using PIMP::Button;
using PIMP::ButtonEvent;
using PIMP::SHORT_PRESS; using PIMP::DOUBLE_PRESS;
using PIMP::LONG_PRESS;  using PIMP::VERY_LONG_PRESS;
using PIMP::LED;


/*!
 * @brief MediaPlayerMode type enum.
 *
 * The enum value specifies whether the media player is configured as a UPnP
 * media renderer or as an internet radio.
*/
enum MP_MODE_TYPE {
    RENDERER,   /*!< UPnP media renderer. */
    RADIO       /*!< Internet radio. */
};


/// Media player operating mode
static MP_MODE_TYPE mode = RENDERER;


/// List of button events received
static queue<ButtonEvent> button_events;


/*!
 * @brief Button event handler.
 * @param e     Button event.
 */
static void button_event_handler(ButtonEvent e) {
    // add button event to event log
    button_events.push(e);
}


/*!
 * @brief Main entry point of Raspberry Pi power on/off switching application.
 *
 * The application configures a callback to be sent whenever the on/off button
 * is pressed, then enters a loop to process button events.
 * 
 * long press   -   Return to renderer mode if in radio mode, or initiate 
 *                  power down if already in renderer mode.
 *
 * short press  -   Set radio mode or select next internet radio station if
 *                  already in radio mode.
 *
 * @return  0: Successful shutdown, -1: Error condition.
 */
int main() {
    
    // initialise pushbutton on GPIO4 and subscribe a button event handler
    Button power_btn("power_btn", 4, true);
    power_btn.add_event_handler(&button_event_handler);

    // Initialise LED on GPIO17 and switch it on.
    // Setting GPIO17 high also prevents system reset via the "RUN" pin.
    LED power_led("power_led", 17, false, false);
    power_led.on();

    // event handler loop
    bool poweroff = false;
    while (poweroff == false) {
        while (!button_events.empty()) {
            // handle all pending button press events
            ButtonEvent e = button_events.front();
            button_events.pop();
            debug_cerr("ButtonEvent: name=" << e.source->get_name() << " event=" << e.type);
            if (e.source == &power_btn) {
                // power button press events
                switch (e.type) {

                    // button press longer than 1 second:
                    // - returns to renderer mode if in radio mode.
                    // - initiates power off if already in renderer mode.
                    case LONG_PRESS :
                    case VERY_LONG_PRESS :
                        if (mode == RENDERER) {
                            debug_cerr("Shutting down");
                            poweroff = true;
                        }
                        else {
                            mode = RENDERER;
                            system("/home/root/radio.sh stop");
                        }
                        break;
                        
                    // short press switches to radio mode and steps through 
                    // the list of radio stations.
                    case SHORT_PRESS :
                        if (mode == RENDERER) {
                            mode = RADIO;
                            system("/home/root/radio.sh start");
                        }
                        else {                            
                            system("/home/root/radio.sh next");
                        }
                        break;
                        
                    // ignore all other button press events
                    default :
                        break;
                }
            }
        }
        // sleep 10ms to allow other threads to run
        usleep(10000);
    }
    
    // Initiate shutdown: Leave LED on to disable reset
    power_btn.remove_event_handler(&button_event_handler);
    system("espeak -a 50 -s 125 \"Power Off\" --stdout | sox -t wav - -c 2 -t wav - | aplay");
    sync();                     // commit filesystem caches to disk
    system("shutdown -h now");  // poweroff
    return 0;
}

#endif  /* ifndef UNIT_TEST */

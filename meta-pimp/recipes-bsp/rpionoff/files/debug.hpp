/*
 * Power On/Off Switching for Raspberry Pi (rpionoff)
 * Version 1.0.0
 * https://bitbucket.org/tbayley/rpionoff
 *
 * Copyright Tony Bayley 2017.
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file COPYING.BOOST-1_0 or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 *
 * Filename: debug.hpp
 * Created on: 18 Mar 2017
 *
 * Debug printing macros.
 *
 */

#ifndef TONY_BAYLEY_DEBUG_HPP
#define TONY_BAYLEY_DEBUG_HPP

#include <iostream>

// macro to evaluate expression (e.g. cout << ...) for DEBUG build only
#ifdef DEBUG
	#define debug_cout(expression) (std::cout << expression << std::endl)
	#define debug_cerr(expression) (std::cerr << expression << std::endl)
#else
	#define debug_cout(expression)
	#define debug_cerr(expression)
#endif

#endif	// #ifndef TONY_BAYLEY_DEBUG_HPP

/*
 * GPIO.hpp  Created on: 29 Apr 2015
 * Copyright (c) 2015 Derek Molloy (www.derekmolloy.ie)
 * Made available for the book "Exploring Raspberry Pi"
 * See: www.exploringrpi.com
 * Licensed under the EUPL V.1.1
 *
 * Modified on: 1 March 2017
 * Copyright (c) 2017 Tony Bayley
 * Implement more robust switch debouncing on GPIO inputs. Modify and add
 * functions to set and get active high / active low status of GPIO inputs.
 * Implement Doxygen comments.
 *
 * This Software is provided to You under the terms of the European
 * Union Public License (the "EUPL") version 1.1 as published by the
 * European Union. Any use of this Software, other than as authorized
 * under this License is strictly prohibited (to the extent such use
 * is covered by a right of the copyright holder of this Software).
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * For more details, see http://www.derekmolloy.ie/
 */

#ifndef TONY_BAYLEY_GPIO_HPP_
#define TONY_BAYLEY_GPIO_HPP_

#include "timeservice.hpp"

#include<string>
#include<fstream>

using std::string;
using std::ofstream;

#define GPIO_PATH "/sys/class/gpio/"

/*!
* @brief Namespace for Pi Media Player
* @see https://bitbucket.org/tbayley/pimp
*/
namespace PIMP {

/*!
 * @brief GPIO event handler callback type "GPIO_callback_t"
 *
 * @param  val      Return value of the method that generated the callback.
 *                    0: success, -1: failure.
 * @param  target   Object that will receive the callback, if the handler is a
 *                  class method, or NULL if the callback is handled by a
 *                  regular function.
 */
typedef void (*GPIO_callback_t)(int, void*);


/*!
 * @brief GPIO direction enum.
 *
 * The enum value specifies whether the GPIO is configured as an input or
 * output.
 */
enum GPIO_DIRECTION {
    INPUT,      /*!< Input. */
    OUTPUT      /*!< Output. */
};


/*!
 * @brief GPIO value enum.
 *
 * The enum value specifies the logic level that is being input to or output
 * from the GPIO pin.
 */
enum GPIO_VALUE {
    LOW=0,      /*!< Logic Low (0V for active-high logic polarity). */
    HIGH=1      /*!< Logic High (3.3V for active-high logic polarity). */
};


/*!
 * @brief GPIO edge detection enum.
 *
 * The enum value specifies whether the GPIO is configured to generate
 * interrupts on rising edge (logic low to high transition), falling edge
 * (logic high to low transition).
*/
enum GPIO_EDGE {
    NONE,       /*!< Edge detection is disabled. */
    RISING,     /*!< Generate interrupt on rising edge. */
    FALLING,    /*!< Generate interrupt on falling edge. */
    BOTH        /*!< Generate interrupt on both rising and falling edges. */
};


/*!
 *  @brief GPIO class
 *
 * The GPIO class provides methods to access the Raspberry Pi's general purpose
 * inputs and outputs.
 */
class GPIO {

public:

    /*!
     * @brief GPIO class constructor
     *
     * The GPIO constructor exports the GPIO pin to make it available for
     * programmatic input and output.  If auto_dispose == true (the default), 
     * the pin is unexported by the GPIO object destructor to make it available
     * to another process.  If auto_dispose == false the pin is not unexported.
     * This enables a GPIO output to be left permanently in any desired state 
     * until Linux is rebooted.
     *
     * @param  number        GPIO input number (e.g. 11 for GPIO11).
     * @param  auto_dispose  true:  unexport GPIO pin when GPIO object destroyed.
     *                       false: leave GPIO pin exported .
     */
    GPIO(int number, bool auto_dispose=true);


    /// Default constructor is needed for unit tests. Do not use otherwise!
    GPIO() = default;


    /*!
     * @brief GPIO class destructor
     *
     * The GPIO destructor unexports the GPIO pin so that it may be used by
     * other processes for programmatic input and output.
     */
    virtual ~GPIO();            // destructor unexports the pin


    /*!
     * @brief Test if the specified GPIO pin is valid.
     *
     * If the selected GPIO pin cannot be exported (i.e. it does not exist, or
     * is in use by the kernel or a driver), then this predicate returns false
     * and the GPIO object cannot be used.
     *
     * @return true: GPIO pin is valid, false: GPIO pin is not valid.
     */
    virtual bool gpio_is_valid();


    /*!
     * @brief Get the GPIO number (e.g. 12 for GPIO12).
     *
     * @return GPIO number.
     */
    virtual int get_number();


    /*!
     * @brief Configure the GPIO pin as input or output.
     *
     * @param  dir  Direction: INPUT or OUTPUT.
     * @return      0: success, -1: failure.
     */
    virtual int  set_direction(GPIO_DIRECTION dir);


    /*!
     * @brief  Read GPIO pin direction configuration
     *
     * @return  Direction: INPUT or OUTPUT.
     */
    virtual GPIO_DIRECTION get_direction();


    /*!
     * @brief Write to GPIO output
     *
     * @param value     Value to write: Logic LOW or HIGH
     * @return          0: success, -1: failure.
     */
    virtual int  set_value(GPIO_VALUE value);


    /*!
     * @brief Read logic level at GPIO input or output pin.
     * @return      Logic level: LOW or HIGH
     */
    virtual GPIO_VALUE get_value();


    /*!
     * @brief Toggle the GPIO output.
     *
     * If output is initially at logic LOW, set it to logic HIGH,
     * and vice-versa.
     *
     * @return      0: success, -1: failure.
     */
    virtual int  toggle_output();


    /*!
     * @brief Set GPIO input or output logic polarity to active low.
     *
     * The default logic polarity of all GPIO pins at boot time is active high.
     * Call this method to change a pin's polarity to active low.
     *
     * @param   is_low  true: set active low, false: set active high.
     * @return  0: success, -1: failure.
     */
    virtual int  set_active_low(bool is_low=true);


    /*!
     * @brief Set GPIO input or output logic polarity to active high.
     *
     * The default logic polarity of all GPIO pins at boot time is active high,
     * so you probably do not need to call this method!
     *
     * @param   is_high  true: set active high, false: set active low.
     * @return  0: success, -1: failure.
     */
    virtual int  set_active_high(bool is_high=true);   // default is active high


    /*!
     * @brief Check if the GPIO logic polarity is configured to active low.
     *
     * @return   true: active low, false: active high.
     */
    virtual bool get_active_low();


    /*!
     * @brief Check if the GPIO logic polarity is configured to active high.
     *
     * @return   true: active high, false: active low.
     */
    virtual bool get_active_high();


    /*!
     * @brief Set the GPIO input debounce time.
     *
     * Mechanical switch contact "bounce" often results in multiple logic state
     * transitions when the switch is pressed or released.  Debounce time is
     * the time for which the GPIO input must remain in a settled state before
     * a switch transition event is generated.  Typical debounce times are in
     * the range 2 to 20 milliseconds.
     *
     * @param time  Debounce time in milliseconds.
     */
    virtual void set_debounce_time(int time);


    // Advanced output: faster by keeping the stream open (~20x)

    /*!
     * @brief Open an output stream to write data to a GPIO output pin.
     * @return  0: success, -1: failure.
     */
    virtual int  stream_open();


    /*!
     * @brief Write to GPIO output via its output stream.
     *
     * @param value     Value to write: Logic LOW or HIGH
     * @return          0: success, -1: failure.
     */
    virtual int  stream_write(GPIO_VALUE value);


    /*!
     * @brief Close the GPIO output stream.
     * @return  0: success, -1: failure.
     */
    virtual int  stream_close();


    /*!
     * @brief Toggle the GPIO output every "time" milliseconds.
     * @param time  Toggle period in milliseconds.
     * @return  0: success, -1: failure.
     */
    virtual int  toggle_output(int time); // invert output every 'time' ms


    /*!
     * @brief Toggle GPIO every "time" milliseconds, for "number_of_times" times.
     * @param number_of_times   Number of times to toggle the output.
     * @param time              Toggle period in milliseconds.
     * @return                  0: success, -1: failure.
     */
    virtual int  toggle_output(int number_of_times, int time);


    /*!
     * @brief Change the GPIO toggle period, to alter the output frequency.
     * @param time  Toggle period in milliseconds.
     */
    virtual void change_toggle_time(int time);


    /*!
     * @brief Stop toggling the GPIO output.
     */
    virtual void toggle_cancel();


    // Advanced input: detect input edges -- threaded and non-threaded

    /*!
     * @brief Set the edge type that will generate GPIO input interrupts.
     * @param value NONE, RISING, FALLING edge or BOTH.
     * @return 0: success, -1: failure.
     */
    virtual int  set_edge_type(GPIO_EDGE value);


    /*!
     * @brief Read the edge type that will generate GPIO input interrupts.
     * @return  Edge type: NONE, RISING, FALLING edge or BOTH.
     */
    virtual GPIO_EDGE get_edge_type();


    /*!
     * @brief Sleep until the specified GPIO input edge type is detected.
     * @return  0: the edge was detected, -1: failure.
     */
    virtual int  wait_for_edge(); // wait until button is pressed


    /*!
     * @brief Configure an asynchronous callback when the specified GPIO input edge type is detected.
     * @param callback  Pointer to the callback function.
     * @param target    Pointer to the object instance that wishes to receive edge notifications.
     * @return          0: Success.
     *                 -1: Failed to create thread.
     *                 -2: Edge detection process already running.
     */
    virtual int  wait_for_edge(GPIO_callback_t callback, void* target); // threaded callback


    /*!
     * @brief Test if edge detection process is enabled.
     * @return      true:  edge detection process is active.
     *              false: edge detection is disabled.
     */
    virtual bool get_wait_for_edge_enabled();


    /*!
     * @brief Cancel edge detection.
     */
    virtual void wait_for_edge_cancel();


private:
    bool initialised = false;   // set true when GPIO export succeeds
    bool auto_dispose = true;   // set true to unexport GPIO in destructor
    int number = 0;             // GPIO number (e.g. 12 for GPIO12)
    int debounce_time = 0;      // time that input must be stable: default 0ms (no debounce)
    int debounce_poll_time = 5; // input sampling period: default 5ms
    int toggle_period = 100;    // default 100ms: 5Hz square wave output
    int toggle_number = -1;     // default -1 (infinite)
    TimeService time_service;
    ofstream stream;
    pthread_t thread = 0;
    GPIO_callback_t callback_function = NULL;
    void * callback_target = NULL;
    bool thread_running = false;
    string path;
    int write(string path, string filename, string value);
    int write(string path, string filename, int value);
    string read(string path, string filename);
    int export_GPIO();
    int unexport_GPIO();
    friend void* threaded_poll(void *value);
    friend void* threaded_toggle(void *value);
};

void* threaded_poll(void *value);      // callback functions for threads
void* threaded_toggle(void *value);    // callback functions for threads

} /* namespace PIMP */

#endif /* TONY_BAYLEY_GPIO_HPP_ */

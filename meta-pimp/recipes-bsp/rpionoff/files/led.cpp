/*
 * Power On/Off Switching for Raspberry Pi (rpionoff)
 * Version 1.0.0
 * https://bitbucket.org/tbayley/rpionoff
 *
 * Copyright Tony Bayley 2017.
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file COPYING.BOOST-1_0 or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 *
 * Filename:   led.cpp
 * Created on: 10 Mar 2017
 *
 * LED class.  Turn on and off an LED connected to a GPIO output pin.
 *
 */

#include "GPIO.hpp"
#include "led.hpp"

#include <iostream>         // cerr, endl
#include <stdexcept>        // runtime_error
#include <string>

using std::cerr;  using std::endl;
using std::runtime_error;

namespace PIMP
{

// LED class constructor.
LED::LED(std::string name, 
         int gpio_number, 
         bool is_low,
         bool auto_dispose) : name(name),
                              instance_GPIO(gpio_number, auto_dispose) {
    // Configure the GPIO input to which the LED is connected.
    #ifdef UNIT_TEST
    cerr << "LED::LED: Do not use release LED constructor for unit testing." << endl;
    #endif
    if (out_GPIO.gpio_is_valid()) {
        out_GPIO.set_direction(OUTPUT);
        out_GPIO.set_active_low(is_low);
        this->off();    // LED off
    }
    else {
        throw runtime_error("LED::LED: Invalid GPIO number.");
    }
}


// LED class constructor for unit testing
LED::LED(std::string name, GPIO & mock_GPIO) : name(name), out_GPIO(mock_GPIO) {
    #ifndef UNIT_TEST
    cerr << "Button::Button: Do not use unit test LED constructor for release." << endl;
    #endif
}


// Test if the LED object's GPIO pin is valid
bool LED::gpio_is_valid() { return out_GPIO.gpio_is_valid(); }


// Set LED state.
int LED::set_state(LED_STATE state) {
    if (out_GPIO.gpio_is_valid()) {
        return out_GPIO.set_value((GPIO_VALUE)state);
    }
    return -1;
}


// Get LED state.
LED_STATE LED::get_state() {
    LED_STATE state = OFF;
    if (out_GPIO.gpio_is_valid()) {
        state = (LED_STATE)out_GPIO.get_value();
    }
    return state;
}


// Turn LED on
int LED::on() {
    return set_state(ON);
}


// Turn LED off
int LED::off() {
    return set_state(OFF);
}


// Test if LED is on.
bool LED::is_on() {
    return (get_state() == ON);
}


// Test if LED is off.
bool LED::is_off() {
    return (get_state() == OFF);
}


// Get the LED's name.
std::string LED::get_name() {
    return name;
}

} /* namespace PIMP */

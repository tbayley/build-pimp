/*
 * Power On/Off Switching for Raspberry Pi (rpionoff)
 * Version 1.0.0
 * https://bitbucket.org/tbayley/rpionoff
 *
 * Copyright Tony Bayley 2017.
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file COPYING.BOOST-1_0 or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 *
 * Filename: button.hpp
 * Created on: 1 Mar 2017
 *
 * Button handler for Raspberry Pi user interface.
 *
 */

#ifndef TONY_BAYLEY_BUTTON_HPP_
#define TONY_BAYLEY_BUTTON_HPP_

#include "GPIO.hpp"
#include "timeservice.hpp"
#include <list>
#include <string>

/*!
@brief Namespace for Pi Media Player
@see https://bitbucket.org/tbayley/pimp
*/
namespace PIMP
{

/*!
 * @brief ButtonEvent type enum.
 *
 * The enum value specifies the type of button event that has occurred.
*/
enum BUTTON_EVENT_TYPE {
    PRESSED,        /*!< Start of button press. */
    RELEASED,       /*!< End of button press. */
    SHORT_PRESS,    /*!< End of short button press (0 to 1 seconds duration). */
    LONG_PRESS,     /*!< End of long button press (1 to 5 seconds duration). */
    VERY_LONG_PRESS,/*!< End of very long button press (more than 5 seconds duration). */
    DOUBLE_PRESS    /*!< Second button press less than 1 second after SHORT_PRESS. */
};


// Declare Button early to avoid circular reference problem with ButtonEventHandler_t
class Button;


// ButtonEvent struct
struct ButtonEvent {
   /*!
     * @brief ButtonEvent struct constructor
     *
     * @param  type     Button event type.
     * @param  source   Pointer to the Button object that generated the event.
     */
    ButtonEvent(BUTTON_EVENT_TYPE type, Button* source);

    /// Button event type.
    BUTTON_EVENT_TYPE type;

    /// Pointer to the Button object that generated the event.
    Button* source;
};


/*!
 * @brief Button event handler callback type "ButtonEventHandler_t"
 *
 * @param  e     Button event.
 */
typedef void (*ButtonEventHandler_t)(ButtonEvent);


/// Button handler for Raspberry Pi user interface.
class Button {

public:
     /*!
      * @brief Button class static callback from GPIO member object.
      *
      * Edge detection callbacks from the GPIO object cannot directly target class
      * instance methods, so this static method acts as a proxy to relay callbacks
      * to the required Button instance.
      *
      * @param  val              Return value of the GPIO::waitForEdge method.
      *                              0: Edge detection succeeded.
      *                             -1: Edge detection failed.
      * @param  p_instance       Pointer to the targeted Button instance.
      */
     static void GPIO_callback(int val, void * p_instance);


   /*!
     * @brief Button class constructor
     *
     * @param  name           Button name (e.g."on-off", or "play").
     * @param  number         GPIO input number (e.g. 11 for GPIO11).
     * @param  active_low     GPIO logic polarity configuration setting.
     *                          true: active low.
     *                          false: active high (default),
     * @param  debounce_time  Switch debounce time in milliseconds (default 10 ms).
     */
    Button(std::string name,
           int number,
           bool active_low = false,
           int debounce_time = 10);


   /*!
     * @brief Button class constructor for unit testing
     *
     * @param name                  Button name (e.g."on-off", or "play").
     * @param mock_GPIO             Reference to mock GPIO object.
     * @param mock_time_service     Reference to mock TimeService object.
     */
    Button(std::string name,
           GPIO & mock_GPIO,
           TimeService &mock_time_service);


    /*!
     * @brief Button class destructor
     *
     * Stop the thread that generates GPIO edge detection events.
     */
    virtual ~Button();


    /*!
     * @brief  Add a Button event handler.
     *
     * When this button generates a Button event, the specified Button event
     * handler function will be called.
     *
     * @param  handler  Pointer to button event handler function.
     * @return          Result:
     *                    0: handler was added.
     *                   -1: Button GPIO is not valid.
     *                   -2: handler already in list.
     */
    virtual int add_event_handler(ButtonEventHandler_t handler);


    /*!
     * @brief  Remove a Button event handler.
     *
     * @param  handler  Pointer to Button Event handler function.
     * @return          Result:
     *                    0: handler was removed.
     *                   -1: Button GPIO is not valid.
     *                   -2: handler was not in list.
     */
    virtual int remove_event_handler(ButtonEventHandler_t handler);


    /*!
     * @brief Get the current button state.
     *
     * @return Button state: HIGH if button is being pressed, else LOW.
     */
    virtual GPIO_VALUE get_state();


    /*!
     * @brief Get the button's GPIO number (e.g. 12 for GPIO12).
     *
     * @return GPIO number, or -1 if GPIO is not valid.
     */
    virtual int get_GPIO_number();


    /*!
     * @brief Get the button's name.
     *
     * @return Button name.
     */
    std::string get_name();


    /*!
     * @brief Get the button's GPIO logic polarity configuration setting.
     *
     * @return true: active low, false: active high.
     */
    virtual bool get_active_low();


    /*!
     * @brief  GPIO edge detection event handler.
     *
     * @param  val Return value of the GPIO::waitForEdge method.
     *                0: Edge detection succeeded.
     *               -1: Edge detection failed.
     */
    void GPIO_event_handler(int val);


    /*!
     * @brief Test if the Button's GPIO pin is valid.
     *
     * If the selected GPIO pin cannot be exported (i.e. it does not exist, or
     * is in use by the kernel or a driver), then this predicate returns false
     * and the Button object cannot be used.
     *
     * @return true: GPIO pin is valid, false: GPIO pin is not valid.
     */
    virtual bool gpio_is_valid();


private:
    /// Button name (e.g."on-off", or "play").
    std::string name;

    /// Pointer to button event handler function.
    std::list<ButtonEventHandler_t> handlers;

    /// State of GPIO input last time it was read.
    GPIO_VALUE stored_state = LOW;

    /// GPIO object that gives access to GPIO hardware input.
    GPIO instance_GPIO;

    /*!
     * @brief  Reference to GPIO input object.
     *
     * A reference is used so that the Button instance's internal GPIO object can be
     * replaced by an external mock GPIO object for unit testing.
     */
    GPIO & in_GPIO = instance_GPIO;

    /// Time service to provide elapsed time values
    TimeService instance_time_service;

    /*!
     * @brief  Reference to TimeService object.
     *
     * A reference is used so that the Button instance's internal TimeService
     * object can be replaced by an external mock TimeService object for unit
     * testing.
     */
    TimeService &time_service = instance_time_service;

    /// Time at which the button was pressed.
    double t_press = -30.0;

    /// Previous time at which the button was pressed: to detect DOUBLE_PRESS
    double t_press_previous = -60.0;

    /// Duration of button press, in seconds.
    double pressed_duration = 0.0;


    /*!
     * @brief  Enable GPIO input edge detection process.
     *
     * The edge detection process runs continuously in a background thread,
     * until it is stopped by calling stop_GPIO_edge_detection(). This occurs
     * either when the last event handlers is unsubscribed, or in the Button 
     * destructor.
     */
    void enable_GPIO_edge_detection();


    /*!
     * @brief  Disable GPIO input edge detection process.
     */
    void disable_GPIO_edge_detection();


    /*!
     * @brief  Send button event notification to subscribed event handlers.
     *
     * @param  e    Button event type.
     */
    void send_button_event(BUTTON_EVENT_TYPE e);


};

} /* namespace PIMP */

#endif  /* TONY_BAYLEY_BUTTON_HPP_ */

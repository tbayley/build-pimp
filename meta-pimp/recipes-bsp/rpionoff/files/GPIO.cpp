/*
 * GPIO.cpp  Created on: 29 Apr 2015
 * Copyright (c) 2015 Derek Molloy (www.derekmolloy.ie)
 * Made available for the book "Exploring Raspberry Pi"
 * See: www.exploringrpi.com
 * Licensed under the EUPL V.1.1
 *
 * Modified on: 1 March 2017
 * Copyright (c) 2017 Tony Bayley
 * Implement more robust switch debouncing on GPIO inputs. Modify and add
 * functions to set and get active high / active low status of GPIO inputs.
 * Implement Doxygen comments.
 *
 * This Software is provided to You under the terms of the European
 * Union Public License (the "EUPL") version 1.1 as published by the
 * European Union. Any use of this Software, other than as authorized
 * under this License is strictly prohibited (to the extent such use
 * is covered by a right of the copyright holder of this Software).
 *
 * This Software is provided under the License on an "AS IS" basis and
 * without warranties of any kind concerning the Software, including
 * without limitation merchantability, fitness for a particular purpose,
 * absence of defects or errors, accuracy, and non-infringement of
 * intellectual property rights other than copyright. This disclaimer
 * of warranty is an essential part of the License and a condition for
 * the grant of any rights to this Software.
 *
 * For more details, see http://www.derekmolloy.ie/
 */

#include "GPIO.hpp"

#include <iostream>
#include <fstream>
#include <stdio.h>          // perror
#include <string>
#include <sstream>
#include <cstdlib>
#include <cstdio>
#include <fcntl.h>
#include <sys/epoll.h>
#include <pthread.h>

using namespace std;

/*!
@brief Namespace for Pi Media Player
@see https://bitbucket.org/tbayley/pimp
*/
namespace PIMP {

// PUBLIC METHODS

// Constructor
GPIO::GPIO(int number,
           bool auto_dispose) : number(number), auto_dispose(auto_dispose) {
    ostringstream s;
    s << "gpio" << number;
    string id = string(s.str());
    this->path = GPIO_PATH + id + "/";
    if (this->export_GPIO() == 0) {
        this->initialised = true;
        // need to give Linux time to set up the sysfs structure
        this->time_service.usleep(250000); // 250ms delay
    }
    else {
        perror("GPIO::GPIO: Failed to export GPIO.");
    }
}


// GPIO destructor
GPIO::~GPIO() {
    // suppress writing to hardware for unit tests
    #ifndef UNIT_TEST
        if (auto_dispose) {
            this->unexport_GPIO();
        }
    #endif
}


// General Input and Output Settings

bool GPIO::gpio_is_valid() { 
    return initialised;
}


int GPIO::get_number() {
    return number;
}


int GPIO::set_direction(GPIO_DIRECTION dir) {
    switch(dir){
        case INPUT: 
            return this->write(this->path, "direction", "in");
            break;
        case OUTPUT:
            return this->write(this->path, "direction", "out");
            break;
    }
    return -1;
}


GPIO_DIRECTION GPIO::get_direction() {
    string input = this->read(this->path, "direction");
    if (input == "in") {
        return INPUT;
    }
    else {
        return OUTPUT;
    }
}


int GPIO::set_value(GPIO_VALUE value) {
    switch(value){
    case HIGH: 
        return this->write(this->path, "value", "1");
        break;
     case LOW: 
        return this->write(this->path, "value", "0");
        break;
    }
    return -1;
}


GPIO_VALUE GPIO::get_value() {
    string input = this->read(this->path, "value");
    if (input == "0") {
        return LOW;
    }
    else {
        return HIGH;
    }
}


int GPIO::toggle_output() {
    if (HIGH == this->get_value()) {
        this->set_value(LOW);
    }
    else {
        this->set_value(HIGH);
    }
    return 0;
}


int GPIO::set_active_low(bool is_low) {
    if(is_low) {
        return this->write(this->path, "active_low", "1");
    }
    else {
        return this->write(this->path, "active_low", "0");
    }
}


int GPIO::set_active_high(bool is_high) {
    return this->set_active_low(!is_high);
}


bool GPIO::get_active_low() {
    string input = this->read(this->path, "active_low");
    if (input == "1") {
        return true;
    }
    else {
        return false;
    }
}


bool GPIO::get_active_high() {
    return !(this->get_active_low());
}


void GPIO::set_debounce_time(int time) {
    this->debounce_time = time;
}


// Advanced output: faster by keeping the stream open (~20x)

int GPIO::stream_open() {
    stream.open((path + "value").c_str());
    return 0;
}


int GPIO::stream_write(GPIO_VALUE value) {
    stream << value << std::flush;
    return 0;
}


int GPIO::stream_close() {
    stream.close();
    return 0;
}


int GPIO::toggle_output(int time) {
    return this->toggle_output(-1, time);
}


int GPIO::toggle_output(int number_of_times, int time) {
    this->set_direction(OUTPUT);
    this->toggle_number = number_of_times;
    this->toggle_period = time;
    this->thread_running = true;
    if(pthread_create(&this->thread, NULL, &threaded_toggle, static_cast<void*>(this))) {
        perror("GPIO::toggle_output: Failed to create the toggle thread");
        this->thread_running = false;
        return -1;
    }
    return 0;
}


void GPIO::change_toggle_time(int time) {
    this->toggle_period = time;
}


void GPIO::toggle_cancel() {
    this->thread_running = false;
}


// Advanced input: detect input edges -- threaded and non-threaded

int GPIO::set_edge_type(GPIO_EDGE value) {
    switch(value){
        case NONE: 
            return this->write(this->path, "edge", "none");
            break;
        case RISING: 
            return this->write(this->path, "edge", "rising");
            break;
        case FALLING: 
            return this->write(this->path, "edge", "falling");
            break;
        case BOTH: 
            return this->write(this->path, "edge", "both");
            break;
    }
    return -1;
}


GPIO_EDGE GPIO::get_edge_type() {
    string input = this->read(this->path, "edge");
    if (input == "rising") {
        return RISING;
    }
    else if (input == "falling") {
        return FALLING;
    }
    else if (input == "both") {
        return BOTH;
    }
    else {
        return NONE;
    }
}


// Blocking Poll - based on the epoll socket code in the epoll man page
int GPIO::wait_for_edge() {
    this->set_direction(INPUT); // must be an input pin to poll its value
    int fd, i, epollfd, count=0;
    struct epoll_event ev;
    epollfd = epoll_create(1);
    if (epollfd == -1) {
       perror("GPIO::wait_for_edge: Failed to create epollfd");
       return -1;
    }
    if ((fd = open((this->path + "value").c_str(), O_RDONLY | O_NONBLOCK)) == -1) {
       perror("GPIO::wait_for_edge: Failed to open file");
       return -1;
    }

    //ev.events = read operation | edge triggered | urgent data
    ev.events = EPOLLIN | EPOLLET | EPOLLPRI;
    ev.data.fd = fd;    // attach the file file descriptor

    //Register the file descriptor on the epoll instance, see: man epoll_ctl
    if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &ev) == -1) {
       perror("GPIO::wait_for_edge: Failed to add control interface");
       return -1;
    }
    while(count<=1) {   // ignore the first trigger
        i = epoll_wait(epollfd, &ev, 1, -1);
        if (i==-1) {
            perror("GPIO::wait_for_edge: Poll Wait fail");
            count=5;    // terminate loop
        }
        else {
            count++;    // count the triggers up
        }
    }
    close(fd);
    if (count==5) return -1;
    return 0;
}


int GPIO::wait_for_edge(GPIO_callback_t callback, void* target) {
    if (this->thread_running) {
        // error: edge detection process is already running
        return -2;
    }
    else {
        // start edge detection process if not already running
        this->thread_running = true;
        this->callback_function = callback;
        this->callback_target = target;
        // create the thread, pass the reference, address of the function and data
        if(pthread_create(&this->thread, NULL, &threaded_poll, static_cast<void*>(this))) {
            // error: failed to create thread
            perror("GPIO::wait_for_edge: Failed to create the poll thread");
            this->thread_running = false;
            return -1;
        }
        return 0;
    }
}


// Test if edge detection process is enabled.
bool GPIO::get_wait_for_edge_enabled() {
    return this->thread_running;
}

    
void GPIO::wait_for_edge_cancel() {
    this->thread_running = false;
}


// PRIVATE METHODS

int GPIO::write(string path, string filename, string value) {
   ofstream fs;
   fs.open((path + filename).c_str());
   if (!fs.is_open()) {
       perror("GPIO::write failed to open file ");
       return -1;
   }
   fs << value;
   fs.close();
   return 0;
}


int GPIO::write(string path, string filename, int value) {
   stringstream s;
   s << value;
   return this->write(path,filename,s.str());
}


string GPIO::read(string path, string filename) {
    ifstream fs;
    fs.open((path + filename).c_str());
    if (!fs.is_open()) {
        perror("GPIO::read failed to open file ");
    }
    string input;
    getline(fs,input);
    fs.close();
    return input;
}


int GPIO::export_GPIO() {
    return this->write(GPIO_PATH, "export", this->number);
}


int GPIO::unexport_GPIO() {
    return this->write(GPIO_PATH, "unexport", this->number);
}


// This thread function is a friend function of the class
void* threaded_toggle(void *value) {
    GPIO *gpio = static_cast<GPIO*>(value);
    bool is_high = (bool) gpio->get_value(); //find current value
    while(gpio->thread_running) {
        if (is_high) {
            gpio->set_value(HIGH);
        }
        else {
            gpio->set_value(LOW);
        }
        gpio->time_service.usleep(gpio->toggle_period * 500);
        is_high = !is_high;
        if(gpio->toggle_number > 0) {
            gpio->toggle_number--;
        }
        if(gpio->toggle_number == 0) {
            gpio->thread_running = false;
        }
    }
    return 0;
}


// This thread function is a friend function of the class
void* threaded_poll(void *value) {
    GPIO *gpio = static_cast<GPIO*>(value);
    GPIO_VALUE state;
    int waitForEdgeValue;
    double t_changed, t_now;
    while(gpio->thread_running){
        // wait for change in GPIO input data
        waitForEdgeValue = gpio->wait_for_edge(); // 0: success, -1: failure
        if (waitForEdgeValue == 0) {
            state = gpio->get_value();
            // start debounce process
            double debounce_time_sec = gpio->debounce_time / 1000.0;
            t_now = gpio->time_service.get_elapsed_time();
            t_changed = t_now;
            while ((t_now - t_changed) < debounce_time_sec) {
                // wait until GPIO state remains stable for debounce_time
                gpio->time_service.usleep(gpio->debounce_poll_time * 1000);
                t_now = gpio->time_service.get_elapsed_time();
                GPIO_VALUE new_state = gpio->get_value();
                if (new_state != state) {
                    // restart debounce process if GPIO state changes
                    state = new_state;
                    t_changed = t_now;
                }
            }
        }
        gpio->callback_function(waitForEdgeValue, gpio->callback_target);
    }
    return 0;
}


} /* namespace PIMP */

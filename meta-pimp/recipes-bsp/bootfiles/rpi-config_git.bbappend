# Modify /boot/config.txt

do_deploy_append() {
    # Load the hifiberry-dac module to use pHAT DAC audio
    echo "# Enable hifiberry-dac or pHAT DAC audio" >>${DEPLOYDIR}/bcm2835-bootfiles/config.txt
    echo "dtoverlay=hifiberry-dac" >>${DEPLOYDIR}/bcm2835-bootfiles/config.txt
}


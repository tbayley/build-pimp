DESCRIPTION = "Install firmware files required by device drivers"

# Firmware for Broadcom WiFi driver (brcmfmac) is not installed by default
# when the kernel is configured to include this driver module.  Therefore it
# must be explicitly installed by this recipe.

LICENSE = "Broadcom"
LIC_FILES_CHKSUM = "\
    file://${WORKDIR}/git/LICENCE.broadcom_bcm43xx;md5=3160c14df7228891b868060e1951dfbc"

PV = "0.1+git${SRCPV}"

# Upstream does not yet publish any release so we have to fetch last working version from GIT
# SRCREV specifies the commit hash of the required git version (Dec 7 2016)
SRCREV = "12987cadb37de28719990dfc2397ec6d09e10566"
SRC_URI = "\
    git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git;protocol=https \
"

do_install () {
    install -d ${D}${base_libdir}/firmware/brcm
    install -m 755 ${WORKDIR}/git/brcm/brcmfmac43143.bin ${D}${base_libdir}/firmware/brcm
}

FILES_${PN} += "\
    ${base_libdir}/firmware/brcm/brcmfmac43143.bin \
"


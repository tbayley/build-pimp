SUMMARY = "Add users"
SECTION = "admin"
PR = "r0"

DESCRIPTION = "Add user accounts, following the example in \
               poky/meta-skeleton/recipes-skeleton/useradd/useradd-example.bb"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=4d92cd373abda3937c2bc47fbc49d690 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

S = "${WORKDIR}"

inherit useradd

# You must set USERADD_PACKAGES when you inherit useradd. This
# lists which output packages will include the user/group creation code.
USERADD_PACKAGES = "${PN}"

# You must also set USERADD_PARAM and/or GROUPADD_PARAM 
# when you inherit useradd.

# USERADD_PARAM specifies command line options to pass to the
# useradd command. Multiple users can be created by separating
# the commands with a semicolon. Here we'll create one user, upmpdcli, with no
# home directory and without shell login (i.e. /sbin/nologin shell):
USERADD_PARAM_${PN} = "-M -r -s /sbin/nologin upmpdcli"

# Prevents do_package failures with:
# debugsources.list: No such file or directory:
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"

# Prevents do_rootfs failures if there are no files to be added to rootfs
ALLOW_EMPTY_${PN} = "1"


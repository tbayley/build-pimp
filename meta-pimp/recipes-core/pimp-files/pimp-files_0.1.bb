SUMMARY = "Miscellaneous files for Pi Media Player (PiMP)"
DESCRIPTION = "Add miscellaneous files to the rootfs, including audio test files"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
PR = "r1"

# Dependencies: gdbserver is needed to run the shell scripts
#               espeak is used for radio.sh script UI
RDEPENDS_${PN} = "gdbserver espeak sox"

SRC_URI = "file://asound.conf \
           file://debug.sh \
           file://piano.aif \
           file://piano.mp3 \
           file://piano.wav \
           file://radio.sh \
          "

do_install () {
    # script to start remote debugging session
    install -d ${D}/home/root
    install -m 755 ${WORKDIR}/debug.sh ${D}/home/root/debug.sh
    # internet radio script, using mpd
    install -m 755 ${WORKDIR}/radio.sh ${D}/home/root/radio.sh
    # audio file examples
    install -d ${D}/home/root/music
    install -m 755 ${WORKDIR}/piano.aif ${D}/home/root/music/piano.aif
    install -m 755 ${WORKDIR}/piano.mp3 ${D}/home/root/music/piano.mp3
    install -m 755 ${WORKDIR}/piano.wav ${D}/home/root/music/piano.wav
    # ALSA sound card configuration
    install -d ${D}${sysconfdir}
    install -m 755 ${WORKDIR}/asound.conf ${D}${sysconfdir}/asound.conf
}

FILES_${PN} += " \
    /home/root/music/piano.aif \
    /home/root/music/piano.mp3 \
    /home/root/music/piano.wav \
    /home/root/radio.sh \
    /home/root/debug.sh \
    ${sysconfdir}/asound.conf \
"


#!/bin/sh
# Shell script to play internet radio stations

# An earlier version of this script played the station announcement directly
# to the sound card using espeak. This causes intermittent "ERROR: Failed to
# open "My ALSA Device" (alsa)", which I think is caused by espeak running as
# the current user and mpc running as user 'mpd'. The fault could be cleared by 
# executing the 'sudo mpd --kill' command.
#
# The problem can be avoided by using espeak to render speech to a wave file.
# Then mpc adds this file to the playlist. To enable local audio files to be
# added to the mpd playlist, mpd.conf must be edited to include the line:
#
#   bind_to_address     "/run/mpd/socket"
#
# The environment is then configured to point to that socket:
#
#   export MPD_HOST=/run/mpd/socket


STATION_ID=$1
DEFAULT_STATION_ID="6"

# Read the last radio station from file.
restore_station() {
    if [ -e /tmp/radio_station ]; then
        # read file from temporary file system (for this session) if it exists
        STATION_ID=`cat /tmp/radio_station`
    elif [ -e ./radio_station ]; then
        # otherwise read previous session's file from non-volatile storage
        STATION_ID=`cat ./radio_station`
    else
        # default
        STATION_ID="$DEFAULT_STATION_ID"
    fi
}


# Write the currently-playing radio station to file.
#
# While the radio is playing, the station is stored in temporary filesystem
# (RAM disk for Yocto Project) to minimise flash memory wear. It is only saved
# to non-volatile storage, that will survive power cycling, when the radio 
# stops playing.
save_station() {
    if [ "$1" = "nv" ]; then
        # save to non-volatile file-system that will survive power-off
        # note that /var/tmp is NOT non-volatile on Yocto Project, so save to CWD
        echo "$STATION_ID" > ./radio_station
    else
        # save to temporary filesystem
        echo "$STATION_ID" > /tmp/radio_station
    fi
}


# Handle "stop"
if [ "stop" = "$STATION_ID" ]; then
    # Save the currently-playing station to non-volatile storage
    restore_station
    save_station nv
    # Stop playing
    echo "Radio off"
    mpc stop
    mpc clear
    sleep 0.3
    espeak -a 50 -s 125 -w /tmp/radio-station.wav "radio off"
    export MPD_HOST=/run/mpd/socket
    mpc add "file:///tmp/radio-station.wav"
    mpc play
    exit
fi

# Handle "start" and "next", both of which modify the radio station ID
if [ "start" = "$STATION_ID" ]; then
    restore_station
elif [ "next" = "$STATION_ID" ]; then
    restore_station
    if [ "1" = "$STATION_ID" ]; then
        STATION_ID="2"
    elif [ "2" = "$STATION_ID" ]; then
        STATION_ID="3"
    elif [ "3" = "$STATION_ID" ]; then
        STATION_ID="4"
    elif [ "4" = "$STATION_ID" ]; then
        STATION_ID="5"
    elif [ "5" = "$STATION_ID" ]; then
        STATION_ID="6"
    elif [ "6" = "$STATION_ID" ]; then
        STATION_ID="celtic"
    elif [ "celtic" = "$STATION_ID" ]; then
        STATION_ID="classic"
    elif [ "classic" = "$STATION_ID" ]; then
        STATION_ID="heart"
    elif [ "heart" = "$STATION_ID" ]; then
        STATION_ID="wiltshire"
    elif [ "wiltshire" = "$STATION_ID" ]; then
        STATION_ID="jazz"
    elif [ "jazz" = "$STATION_ID" ]; then
        STATION_ID="kerrang"
    elif [ "kerrang" = "$STATION_ID" ]; then
        STATION_ID="whip"
    elif [ "whip" = "$STATION_ID" ]; then
        STATION_ID="world"
    elif [ "world" = "$STATION_ID" ]; then
        STATION_ID="1"
    else
        # default
        STATION_ID="$DEFAULT_STATION_ID"
    fi
fi

# Play the selected station
# The URIs were obtained from http://www.radiofeeds.co.uk/other.asp and https://www.internet-radio.com/
# Last updated November 2023
if [ "1" = "$STATION_ID" ]; then
    STATION_NAME="BBC Radio 1"
    # URI="http://as-hls-ww-live.akamaized.net/pool_904/live/ww/bbc_radio_one/bbc_radio_one.isml/bbc_radio_one-audio%3d96000.norewind.m3u8" # 96k
    URI="http://as-hls-uk-live.akamaized.net/pool_904/live/uk/bbc_radio_one/bbc_radio_one.isml/bbc_radio_one-audio%3d320000.norewind.m3u8" # 320k
elif [ "2" = "$STATION_ID" ]; then
    STATION_NAME="BBC Radio 2"
    # URI="http://as-hls-ww-live.akamaized.net/pool_904/live/ww/bbc_radio_two/bbc_radio_two.isml/bbc_radio_two-audio%3d96000.norewind.m3u8" # 96k
    URI="http://as-hls-ww-live.akamaized.net/pool_904/live/ww/bbc_radio_two/bbc_radio_two.isml/bbc_radio_two-audio%3d320000.norewind.m3u8" # 320k
elif [ "3" = "$STATION_ID" ]; then
    STATION_NAME="BBC Radio 3"
    # URI="http://as-hls-ww-live.akamaized.net/pool_904/live/ww/bbc_radio_three/bbc_radio_three.isml/bbc_radio_three-audio%3d96000.norewind.m3u8" # 96k
    URI="http://as-hls-ww-live.akamaized.net/pool_904/live/ww/bbc_radio_three/bbc_radio_three.isml/bbc_radio_three-audio%3d320000.norewind.m3u8" # 320k
elif [ "4" = "$STATION_ID" ]; then
    STATION_NAME="BBC Radio 4"
    # URI="http://as-hls-ww-live.akamaized.net/pool_904/live/ww/bbc_radio_fourfm/bbc_radio_fourfm.isml/bbc_radio_fourfm-audio%3d96000.norewind.m3u8" # 96k
    URI="http://as-hls-uk-live.akamaized.net/pool_904/live/uk/bbc_radio_fourfm/bbc_radio_fourfm.isml/bbc_radio_fourfm-audio%3d320000.norewind.m3u8" # 320k
elif [ "5" = "$STATION_ID" ]; then
    STATION_NAME="BBC 5 Live"
    # URI="http://as-hls-uk-live.akamaized.net/pool_904/live/uk/bbc_radio_five_live/bbc_radio_five_live.isml/bbc_radio_five_live-audio%3d96000.norewind.m3u8" # 96k
    URI="http://as-hls-uk-live.akamaized.net/pool_904/live/uk/bbc_radio_five_live/bbc_radio_five_live.isml/bbc_radio_five_live-audio%3d320000.norewind.m3u8" # 320k
elif [ "6" = "$STATION_ID" ]; then
    STATION_NAME="BBC 6 Music"
    # URI="http://as-hls-ww-live.akamaized.net/pool_904/live/ww/bbc_6music/bbc_6music.isml/bbc_6music-audio%3d96000.norewind.m3u8" # 96k
    URI="http://as-hls-uk-live.akamaized.net/pool_904/live/uk/bbc_6music/bbc_6music.isml/bbc_6music-audio%3d320000.norewind.m3u8" # 320k
elif [ "celtic" = "$STATION_ID" ]; then
    STATION_NAME="Celtic Music Radio"
    # URI="https://streaming.broadcastradio.com:11130/celtic" # MP3 128k HTTPS
    URI="http://uksouth.streaming.broadcast.radio:11130/celtic" # MP3 128k
elif [ "classic" = "$STATION_ID" ]; then
    STATION_NAME="Classic FM"
    # URI="https://icecast.thisisdax.com/ClassicFM" # 48k AAC
    URI="https://icecast.thisisdax.com/ClassicFMMP3" # MP3 128k
elif [ "heart" = "$STATION_ID" ]; then
    STATION_NAME="Heart (Wiltshire)"
    # URI="https://icecast.thisisdax.com/HeartWiltshire" # 48k AAC
    URI="https://icecast.thisisdax.com/HeartWiltshireMP3" # 128k MP3
elif [ "wiltshire" = "$STATION_ID" ]; then
    STATION_NAME="BBC Radio Wiltshire"
    URI="http://as-hls-ww-live.akamaized.net/pool_904/live/ww/bbc_radio_wiltshire/bbc_radio_wiltshire.isml/bbc_radio_wiltshire-audio%3d320000.norewind.m3u8" # 320k
elif [ "jazz" = "$STATION_ID" ]; then
    STATION_NAME="Jazz FM"
    URI="http://edge-bauerall-01-gos2.sharp-stream.com/jazzhigh.aac?aw_0_1st.skey=1699622111" # 128k AAC
    # URI="http://edge-bauerall-01-gos2.sharp-stream.com/jazz.mp3?aw_0_1st.skey=1699622203" # 112k MP3
elif [ "kerrang" = "$STATION_ID" ]; then
    STATION_NAME="Kerrang Radio"
    URI="http://edge-bauerall-01-gos2.sharp-stream.com/kerrang.mp3?aw_0_1st.skey=1699622375" # 112k MP3
    # URI="http://edge-bauerall-01-gos2.sharp-stream.com/kerrang.aac?aw_0_1st.skey=1699622467" # 48k AAC
elif [ "whip" = "$STATION_ID" ]; then
    STATION_NAME="The Whip Radio"
    URI="https://centova2.cheapshoutcast.com/proxy/thewhip?mp=/stream"
elif [ "world" = "$STATION_ID" ]; then
    STATION_NAME="BBC World Service"
    # URI="http://stream.live.vc.bbcmedia.co.uk/bbc_world_service" # 56k MP3
    URI="http://as-hls-ww-live.akamaized.net/pool_904/live/ww/bbc_world_service/bbc_world_service.isml/bbc_world_service-audio%3d96000.norewind.m3u8" # 96k
else
    # Print help
    echo "Usage:"
    echo "  radio.sh ID    -  Play radio with given station ID"
    echo "  radio.sh start -  Start playing the last station played"
    echo "  radio.sh stop  -  Stop playing"
    echo "  radio.sh next  -  Play next radio station in Station ID list"
    echo "  radio.sh help  -  Show this help message"
    echo
    echo "Station ID:  1         -  BBC Radio 1"
    echo "             2         -  BBC Radio 2"
    echo "             3         -  BBC Radio 3"
    echo "             4         -  BBC Radio 4"
    echo "             5         -  BBC Radio 5 Live"
    echo "             6         -  BBC 6 Music"
    echo "             celtic    -  Celtic Music Radio"
    echo "             classic   -  Classic FM"
    echo "             heart     -  Heart (Wiltshire)"
    echo "             wiltshire -  Heart (Wiltshire)"
    echo "             jazz      -  Jazz FM"
    echo "             kerrang   -  Kerrang! Radio"
    echo "             world     -  BBC World Service"
    exit
fi

# save the radio station to file
save_station

# clear the currently-playing MPD playlist
mpc stop
mpc clear

# create station name announcement in /tmp directory
# Yocto Project implements /tmp as a RAM disk (tmpfs), so files are volatile
echo "Playing $STATION_NAME"
espeak -m -a 50 -s 125 -w /tmp/radio-station.wav "$STATION_NAME <break time=\"1000ms\"/>"
export MPD_HOST=/run/mpd/socket
mpc add "file:///tmp/radio-station.wav"

# play the announcement and the selected station using mpc / mpd
mpc add $URI
mpc play
exit

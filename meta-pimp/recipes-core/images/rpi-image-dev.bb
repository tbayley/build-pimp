SUMMARY = "A core image for Raspberry Pi development and debugging."
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DESCRIPTION = "A core image for Raspberry Pi development and debugging. \
               The board has a USB WiFi dongle fitted, but no HAT plug-in \
               boards"

# Base this image on rpi-hwup-image
require recipes-core/images/rpi-hwup-image.bb

# Add image features
# Available features listed in poky/meta/classes/core-image.bbclass
IMAGE_FEATURES += "ssh-server-openssh tools-debug"

# Install firmware required by Broadcom WiFi dongle driver (brcmfmac)
IMAGE_INSTALL += "firmware"

# Add packages
IMAGE_INSTALL += " \
  avahi \
  nano \
  pimp-files \
  rpionoff \
  useradd \
  wpa-supplicant \
"


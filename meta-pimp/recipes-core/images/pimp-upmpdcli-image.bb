SUMMARY = "A core image for Pi Media Player (PiMP), using upmpdcli"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

DESCRIPTION = "A core image for a fully-featured version of Pi Media Player \
               (PiMP), with user interface implemented using push-buttons, \
               LCD character display and rotary encoders.  This image \
               implements the UPnP media renderer using upmpdcli."

# Base this image on rpi-hwup-image
require recipes-core/images/rpi-hwup-image.bb

# Add image features
# Available features listed in poky/meta/classes/core-image.bbclass
IMAGE_FEATURES += "ssh-server-openssh"

# Install firmware required by Broadcom WiFi dongle driver (brcmfmac)
IMAGE_INSTALL += "firmware"

# Add packages
IMAGE_INSTALL += " \
  alsa-utils \
  avahi \
  lame \
  mpc \
  mpd \
  nano \
  pimp-files \
  rpionoff \
  sox \
  upmpdcli \
  useradd \
  wpa-supplicant \
"


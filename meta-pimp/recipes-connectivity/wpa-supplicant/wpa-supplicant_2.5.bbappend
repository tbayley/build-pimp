# Prepend the path for the updated /etc/wpa_supplicant.conf file to the original
# file search path, so that the updated file overrides the original one.

FILESEXTRAPATHS_prepend := "${THISDIR}/wpa-supplicant_2.5:"


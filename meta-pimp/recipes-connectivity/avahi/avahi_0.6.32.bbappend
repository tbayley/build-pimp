# Fix "ERROR: pimp-image-1.0-r0 do_rootfs: avahi not found in the feeds".
# This error occurs because the avahi recipe does not generate an output
# called avahi to be installed in the rootfs.  The output is avahi-daemon.

ALLOW_EMPTY_${PN} = "1"
